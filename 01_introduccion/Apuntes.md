# Introducción
Existen diferentes notaciones para representar la estructura de una base de datos(BBDD).

La conversión de esa representación a tablas permite la implementación en un Sistema gestor de base de datos(SGBD).

El SGBD contiene una BBDD con los datos de usuarios y tablas del propio sistema gestor, esto es conocido como el diccionario de datos.

## Definición

Una base de datos es un conjunto de datos con información y las relaciones de esta información.

Existen dos tipos de datos en una BBDD:
- De usuario: Información útil.
- De sistema: Los datos que necesita el SGBD para funcionar.

## Características
- Integridad: Aseguran que los datos que se encuentran en la BBDD son validos.
- Seguridad: Acceso por usuarios autorizados en 3 niveles.
    * Nivel 1: Autenticación de usuarios y permisos de estos.
    * Nivel 2: Cifrado de información.
    * Nivel 3: Control de datos íntegros, consistencia.
- Redundancia: Redundancia mínima.
- Velocidad de acceso: Agilidad , influyen los siguientes factores.
    * Tiempo de conexión, usuarios conectados...
    * Estructura física de la BBDD.
    * Estructura lógica de la BBDD.
    * Optimización de las consultas (orden, índice, claves...).
- Representación de la información.
    * Interfaz de programación.
    * Interfaz de usuario final.
- Migración y compatibilidad.
    - Capacidad de importar y exportar datos en grandes cantidades.
    - compatibilidad con formatos CSV, XML y similares.
- Independencia de datos.
    - Física (independencia del SO y el sistema de ficheros).
    - Lógica (las instrucciones son independientes, modificar o reordenar un campo no afecta a un select).
### Integridad    
Para garantizar la integridad las BBDD tienen un sistema de *transacciones* de forma que solo almacena las transacciones que han finalizado de forma satisfactoria.

La integridad también esta garantizada en SQL por las restricciones (*CONSTRAINT*)  como claves primarias o foráneas.

### Velocidad
La principal optimización posible por parte del administrador de BBDD se encuentra en el momento de la ejecución de la sentencia.

![./mermaid-diagram.svg](./mermaid-diagram.svg)
