# Main
El método o función *main*  sera lo primero en ejecutarse al iniciar un programa y debería contener únicamente la estructura de este.

```java
public static void main(String[] args)
```
se compone de:

- **public**: Accesible por todas las clases.
- **static**: Sin instancia de clases.
- **void**: No devuelve nada.
- **String\[\] args**: Los parámetros que recibe el programa.

# Clases

- Las clases se encuentran separadas en ficheros.
- El nombre del fichero y el de su clase deben coincidir (clase: pedro, fichero: pedro.java)
- En caso de que un fichero contenga varias clases una debe ser public y coincidir con el nombre del fichero.

# Identificadores
- Sólo se pueden utilizar: 
    - letras mayúsculas 
    - letras minúsculas
    - dígitos
    - carácter _
    - carácter $
- No puede comenzar por dígito.
- No puede contener espacios (carácter " ").
- No puede ser igual a una palabra reservada.
- Sensible a mayúsculas y minúsculas.

# Recomendaciones para identificadores
## Clases
Deberán ser uno o varios sustantivos y empezar por mayúscula.
- class ProfesorIES
- class Restaurante
- class BarcoVapor
## Constantes
Deben estar escritos completamente en mayúsculas y separar sus palabras con "_".
- final int TIEMPO_MAX
- final String DIRECCION_BBDD
## Interfaces 
Deberán empezar por mayúscula.
- interface Cloneable
## Métodos
Deberán ser verbos con todo el verbo en minúscula. Se pueden agregar sustantivos con la primera letra en mayúscula. Evitar el uso de "_".
# Comentarios
```java

// comentario de una linea

/* Comentario 
 * de varias
 * lineas */
 
 /** comentario de documentación 
  *  @author Victor-bin */
```
Los comentarios de documentación se colocan antes de la variable o función a documentar. Con javadoc se puede generar documentación HTML en base a estos comentarios.

# Palabras reservadas
|  |  |  |  |  |
| -- | -- | -- | -- | -- |
| abstract | assert | boolean | break | byte |
| case | catch | char | class | <s>const</s> | 
| continue | default | do | double | else |
| enum | extends | false | final | finally |
| float | for | <s>goto</s> | if | implements |
| import | instanceof | int | interface | long |
| native | new | null | package | private |
| protected | public | return | short | static |
| strictfp | super | switch | synchronized | this |
| throw | throws | transient | true | try |
| void | volatile | while |  |  |

*sizeof* no existe debido a que los tipos tienen tamaño fijo en java

*const y goto* pese a existir como palabra clave (por compatibilidad con la primera JVM) después se elimino su implementación dejándolas inservibles.

# Declaración de variables
## Declaración simple
```
<tipo de datos>  <identificador>;
```
## Declaración múltiple
```
<tipo de datos>  <identificador1>, <identificador2>,... ;
```
## Declaración e inicialización:
```
<tipo de datos>  <identificador1> = <valor>;
```
# Recomendaciones para variables 
- No utilizar el carácter "_".
- No crear identificadores que sólo se diferencien por mayúsculas/minúsculas
    - miResultado
    - MiResultado.
- Comenzar por minúscula.
- En un identificador de varias palabras. comenzar la segunda y sucesiva por letra Mayúscula: 
    - mediaAritmetica
    - valorTotal
- No utilizar notación húngara.

# Tipos de datos
 - *int*: Enteros
 - *short*: Enteros cortos
 - *long*: Enteros largos
 - *byte*: Enteros muy cortos (-128 al 127).
 - *float*: Reales
 - *double*: Reales largos
 - *string*: cadena de caracteres
 - *char*: carácter
 - *boolean*: lógico / booleano
 
# Operadores
## Aritméticos
 - Suma: \+
 - Resta: \-
 - Producto: \*
 - División: /
 - Resto: %
 - Incremento: \+\+
 - Decremento: \-\-
 
## Lógicos
 - AND: \&\&
 - OR: ||
 - NOT: \!

## Relacionales
 - Asignación: ==
 - Distinto: \!=
 - Menor: \<
 - Menor o igual: <=
 - Mayor: \>
 - Mayor o igual: >=
 
## Manipulación de bits
 - AND: \&
 - OR: |
 - XOR: ^
 - Complemento a 1: ~
 - Rotación izquierda: <<
 - Rotación derecha: >>

## Asignación
 - Suma: \+=
 - Resta: \-=
 - Producto: \*=
 - División: /=
 - Resto: %=
 - AND: \&=
 - OR: |=
 - XOR: ^=
 - Complemento a 1: ~=
 - Rotación izquierda: <<=
 - Rotación derecha: >>=
 
## Cadenas
 - Suma: \+

## Operador condicional
Similar a un if 

\< condición \> ? \< expresión si la condición se cumple \> : \< expresión si no se cumple \> ;

```java
 Nota >= 5 ? Aprobado = false : Aprobado = true;
``` 
## Operadores de conversión
- ( \< tipo a convertir \> ) \< expresión \>
