# Ficheros

## Java.io

Este paquete proporciona lo necesario para la E/S básica de ficheros estándar.

Esta basado en streams, un flujo de información que contiene los datos a procesar.

## Formas de acceso

- **Secuencial**: para poder ir a un punto n hay que recorrer desde el inicio hasta n-1.
- **Acceso aleatorio**: permiten saltar a la posición concreta, son mucho mas rápidos, requiere indexación.

## Tipos de ficheros

- **Ficheros de texto**  (Caracteres Unicode)
- **Ficheros binarios** (binarios)

## Funcionamiento

- **Gestión de ficheros**:
    La clase ***File*** tiene los siguientes métodos:
    
    - ***getName()***: Devuelve nombre del fichero.
    - ***getPath()***: Devuelve la ruta relativa.
    - ***getAbsolutePath()***: Devuelve la ruta absoluta.
    - ***canRead()***: *True* si es de lectura.
    - ***canWrite()***: *True* si es de escritura.
    - ***length()***: Tamaño del fichero en bytes.
    - ***createNewFile()***: Crea un nuevo fichero asociado a file si no existe un fichero con el nombre indicado.
    - ***delete()***: Borra el fichero asociado.
    - ***exists()***: *True* si existe.
    - ***getParent()***: Devuelve el nombre del directorio padre o *null* si no existe.
    - ***isDirectory()***: *True* si el objeto de File es un directorio.
    - ***isFile()***: *True* si el objeto de File es un fichero.
    - ***mkdir()***: Crea un directorio con el nombre indicado en la creación de File.
    - ***renameTo()***: Cambiar nombre del fichero.

- **Lectura**:
    - FileReader
    - BufferedReader

- **Escritura**:
    - FileWriter
    - BufferedWriter
