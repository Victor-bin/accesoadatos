package ejercicioFileBuffer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author vtienza
 *
 */
public class TablaNombres {
	private List<String> listaNombres;
	
	public TablaNombres() {
		listaNombres = new ArrayList<String>();
	}
	
	public TablaNombres(String nombre) {
		listaNombres = new ArrayList<String>();
		listaNombres.add(nombre);
	}
	
	public TablaNombres(String[] nombres) {
		listaNombres = new ArrayList<String>();
		for (int i = 0; i < nombres.length; i++)
			listaNombres.add(nombres[i]);
	}

	public String mostrarTabla() {
		String result = "";
		for (String cadena : listaNombres)
			result += cadena + "\n";
		return result;
	}
	
	public void vaciarTabla() {
		listaNombres.clear();
	}
	
	public void añadirNombre(String nombre) {
		listaNombres.add(nombre);
	}
	
	public void añadirNombres(String[] nombres) {
		for (int i = 0; i < nombres.length; i++)
			listaNombres.add(nombres[i]);
		
	}
	
	public void guardarEnFichero(String nombreFichero) {
		
		try {
			// create
			BufferedWriter ficheroBW = new BufferedWriter(new FileWriter(new File(nombreFichero)));
			
			// write
			for (String cadena : listaNombres) {
				ficheroBW.write(cadena);
				ficheroBW.newLine();
			}
			
			// close
			ficheroBW.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}		
	}
	
	public void leerDeFichero(String nombreFichero) {
		try {
			// create
			BufferedReader ficheroBR = new BufferedReader(new FileReader(new File(nombreFichero)));
			// read
			String line = ficheroBR.readLine();
			
			while (line != null){
				listaNombres.add(line);
				line = ficheroBR.readLine();
			}
			
			// close
			ficheroBR.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}		
	}	
	

}
