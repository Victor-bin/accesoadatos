package ejercicioFileBuffer;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		final Scanner leerTeclado = new Scanner(System.in);
		String titulo = 
			"Maquina de los numeros\n"
			 + 	"_________________";
		String instruccion = 
				"Las opciones son: \n "
			+	"1-> Mostrar tabla.\n "
			+	"2-> Vaciar la tabla.\n "
			+	"3-> Añadir un nombre.\n "
			+	"4-> Añadir varios nombres.\n "
			+   "5-> Guardar en un fichero.\n "
			+	"6-> Leer de un fichero.\n "
			+   "0-> Salir.\n ";
			
		int opcion = -1;
		int SALIR = 0;//valor de salida
		String nombre;
		String nombreFichero;
		
		//String[] nombres = {"Juan", "Laura", "Eduardo", "Ana"};
		TablaNombres tablaNombres = new TablaNombres();
		
		System.out.println(titulo);
		System.out.println(instruccion);

		try {
			opcion = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			leerTeclado.next();
		}

				
		// bucle menu principal
		do {
			switch (opcion) {
			case 1:
				System.out.println("__ NOMBRES __");
				System.out.println(tablaNombres.mostrarTabla());
			break;
			case 2:
				tablaNombres.vaciarTabla();
				System.out.println("TABLA BORRADA");
			break;
			case 3:
				System.out.print("Nombre: ");
				nombre = leerTeclado.next();
				tablaNombres.añadirNombre(nombre);							
			break;
			case 4:
				System.out.print("Numero de nombres a introducir: ");
				int n = leerTeclado.nextInt();
				for (int i = 0; i < n; i++) {
					System.out.print("Nombre" + (i+1) + ": ");
					nombre = leerTeclado.next();
					tablaNombres.añadirNombre(nombre);
				}
			break;
			case 5:
				System.out.print("Fichero de salida: ");
				nombreFichero = leerTeclado.next();
				tablaNombres.guardarEnFichero(nombreFichero);
			break;
			case 6:
				System.out.print("Fichero de entrada: ");
				nombreFichero = leerTeclado.next();
				tablaNombres.leerDeFichero(nombreFichero);
			break;
			default:
				System.out.println("valor invalido");
			break;
			}
			if (opcion != SALIR ) {// si no ha acabado
				System.out.println("Desea continuar? " + instruccion);// repite las opciones
				try {
					opcion = leerTeclado.nextInt();// lee la opcion
				} catch (Exception e) {
					System.out.println("valor invalido");
					leerTeclado.next();
					opcion = 422;//resetea la opcion
				}
			} 
		} while (opcion != SALIR );
		leerTeclado.close();
	}

}
