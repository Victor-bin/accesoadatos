# Ejercicio agencia viajes
## Diagrama lógico
![./diagrams/classDiagram.svg](./diagrams/classDiagram.svg) 

## Itinerario
La creación de este objeto permite el uso arrays y listas del mismo, además conceptualmente considero que es intuitivo crear un itinerario como objeto común con el que trabajen el resto de clases.

### Atributos
- **nombre** atributo requerido por el enunciado.
- **destinos** esta array (dinámica por comodidad) contendrá tantos destinos como se hayan definido en numeroDestinos.

### Métodos
- **addDestino** asigna un nuevo destino al itinerario
- **getDestino** este método devuelve un destino de la lista destinos, es especialmente necesario para poder ver el destino mas común.
- **getNombre** este método es utilizado para la escritura en el fichero en el formato correspondiente al fichero
- **getNumeroDestinos** este método devuelve el atributo numeroDestinos, es especialmente necesario para ordenar los itinerarios
- **removeDestino** borra un destino del itinerario
- **setNombre​** asigna un nuevo nombre al itinerario
- **toString** String con formato para mostrar al usuario

## ListaItinerarios
Esta clase maneja los itinerarios existentes en memoria, pretendiendo facilitar organización y operaciones básicas y de carácter grupal.

### Atributos
- **listaItinerarios** la propia lista de itinerarios sobre la que trabaja la clase

### Métodos
- **borrar** borra el itinerario de la lista.
- **destinoComun** devuelve el destino que mas veces encuentra
- **getAllDestinos** método privado, devuelve una array con todos los destinos en los itinerarios de la lista
- **getItinerario​** devuelve el Itinerario en la posición indicada
- **getLongitud** Calcula los itinerarios indexados en la lista
- **getMax** método privado, devuelve el numero máximo de destinos en los itinerarios de la lista
- **getMin** método privado, devuelve el numero mínimo de destinos en los itinerarios de la lista
- **insertar** introduce un itinerario a la lista
- **modificar** remplaza un itinerario por otro
- **OrderHighToLow** devuelve una String con los itinerarios ordenados de mayor a menor (por sus destinos)
- **OrderLowToHigh** devuelve una String con los itinerarios ordenados de menor a mayor (por sus destinos)
- **toString** String con formato para mostrar al usuario
- **vaciar** vacía la lista

## FicheroItinerario
Clase con los métodos descritos en el enunciado, maneja la conexión con ficheros que contienen itinerarios

### Atributos
- **nombreFichero** Nombre del fichero a conectar
- **ficheroW** Conexión de escritura
- **ficheroR** Conexión de lectura
- **lineasLeidas** lineas a tratar

### Métodos
- **abrir** Se procederá a abrir el fichero para realizar operaciones sobre él para ello instancia los buffers al fichero con el nombre introducido en el constructor. Si el fichero no existe, se creará vacío.
- **cerrar** Al llamar a este método, se cerrará el fichero previamente abierto.
- **escribir** Al llamar a este método, escribirá en el fichero el itinerario indicado.
- **finalFichero** devolverá true cuando se haya alcanzado el final del fichero. En caso contrario, devolverá false.
- **leer** Lee del fichero un itinerario y lo guarda
- **vaciar** Se borrará el contenido del fichero

## Main
clase estática con el menú que conecta toda las clases. Creada en función del enunciado.
Las opciones son
- Leer fichero.
- Insertar.
- Borrar.
- Vaciar.
- Modificar.
- Guardar fichero.
- Separar fichero.
- Mostrar contenido.
- Mostrar itinerarios de menor a mayo.
- Mostrar itinerarios de mayor a menor.
- Mostrar el destino más repetido de todos los itinerarios.
- Salir.

## Operacion
Este enum corresponde a facilitar el uso del método *modificar* correspondiente a la clase *ListaItinerarios*
