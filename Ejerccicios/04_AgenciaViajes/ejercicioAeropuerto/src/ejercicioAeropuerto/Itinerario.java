package ejercicioAeropuerto;

import java.util.ArrayList;

/**
 * Esta clase instancia un objeto Intinerario y sus metodos
 * @author vtienza
 * @version 1.0
 */
public class Itinerario {
	/**
	 * Nombre del itinerario
	 */
	private String nombre;
	/**
	 * Destinos del itinerario
	 */
	private ArrayList<String> destinos;
	
	/**
	 * Constructor vacio
	 */
	public Itinerario() {
		nombre = null;
		destinos = new ArrayList<String>();
	}
	/**
	 * Constructor basico con todos los atributos del objeto
	 * @param nombre			Nombre del itinerario
	 * @param destinos			Destinos del itinerario
	 */
	public Itinerario(String nombre, ArrayList<String> destinos) {
		this.nombre = nombre;
		this.destinos = destinos;
	}
	
	/**
	 * Devuelve el numero de destinos
	 * @return Devuelve un int con el numero de destinos
	 */
	public int getNumeroDestinos() {
		return destinos.size();
	}
	
	/**
	 * Devuelve el nombre del itinerario
	 * @return Devuelve el nombre del itinerario
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Asigna un nuevo nombre al itinerario
	 * @param nombre	El nuevo nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Asigna un nuevo destino al itinerario
	 * @param destino	El nuevo destino
	 */
	public void addDestino(String destino) {
		destinos.add(destino);
	}
	
	/**
	 * Borra un destino del itinerario
	 * @param destino	El destino a eliminar
	 */
	public void removeDestino(String destino) {
		destinos.remove(destino);
	}
	/**
	 * Devuelve el destino en la posicion indicada
	 * @param posicion 		La posicion del destino
	 * @return Devuelve un String con el destino en la posicion recibida
	 */
	public  String getDestino(int posicion) {
		return destinos.get(posicion);
	}
	
	/**
	 * Devuelve el intinerario 
	 * @return Devuelve un String con el intinerario 
	 */
	@Override
	public String toString() {
		return "\n Itinerario " + nombre + "\n\t Numero de Destinos:\t" + destinos.size() + "\n\t Destinos \t\t" + destinos+"\n";
	}

}