/**
 * 
 */
package ejercicioAeropuerto;

import java.util.ArrayList;

/**
 * Clase para manejar grupos de intinerarios
 * @author vtienza
 * @version 1.0
 */
public class ListaItinerarios {
	/**
	 *	Lista de los itinerarios (Clase Itinerario)
	 */
	private ArrayList<Itinerario> listaItinerarios;

	/**
	 * Instancia la lista, sin parametros
	 */
	public ListaItinerarios() {
		listaItinerarios =  new ArrayList<Itinerario>();
	}
	
	/**
	 * Calcula los itinerarios indexados en la lista
	 * @return el numero de itinerarios
	 */
	public int getLongitud() {
		return listaItinerarios.size();
	}
	
	/**
	 * Devuelve el Itinerario en la posicion indicada
	 * @param posicion 		La posición del Itinerario
	 * @return Devuelve el Itinerario de la posición recibida
	 */
	public Itinerario getItinerario(int posicion) {
		if (posicion < listaItinerarios.size() && posicion > -1)
			return listaItinerarios.get(posicion);
		else
			return null;
	}
	
	/**
	 * Introduce un itinerario a la lista
	 * @param itinerario	El itinerario a introducir
	 */
	public void insertar(Itinerario itinerario) {
		listaItinerarios.add(itinerario);
	}
	
	/**
	 * Borra un itinerario de la lista
	 * @param itinerario	El itinerario a borrar
	 */
	public void borrar(Itinerario itinerario) {
		for (int i = 0; i < listaItinerarios.size(); i++) 
			if(itinerario.equals(listaItinerarios.get(i)))
				listaItinerarios.remove(i);
	}
	
	/**
	 * Vacia la lista de itinerario
	 */
	public void vaciar() {
		listaItinerarios.clear();
	}
	
	/**
	 * Modidifica un itinerario de la lista
	 * @param itinerarioActual	El itinerario a Modificar
	 * @param cambio 	contendra el nuevo nombre / el nuevo destino / el destino a borrar
	 * @param operacion	La operacion a realizar (Del enum Operacion)
	 */
	public void modificar(Itinerario itinerarioActual, String cambio, Operacion operacion) {
		if (operacion == Operacion.ADDDESTINY )
			for (int i = 0; i < listaItinerarios.size(); i++)
				if(listaItinerarios.get(i) == itinerarioActual)
					listaItinerarios.get(i).addDestino(cambio);	
		if (operacion == Operacion.RMVDESTINY )
			for (int i = 0; i < listaItinerarios.size(); i++)
				if(listaItinerarios.get(i) == itinerarioActual)
					listaItinerarios.get(i).removeDestino(cambio);	
		if (operacion == Operacion.MODNAME )
			for (int i = 0; i < listaItinerarios.size(); i++)
				if(listaItinerarios.get(i) == itinerarioActual)
					listaItinerarios.get(i).setNombre(cambio);	
	}
	
	
	/**
	 * Mostrará el contenido de los itinerarios por orden ascendente según el número de destinos.
	 * @return una String con los itinerario
	 */
	public String OrderHighToLow() {
		int max = getMax();
		String text = "";
			
		for (int n = max; n >= getMin(); n--)
			for (int i = 0; i < listaItinerarios.size(); i++)
				if(listaItinerarios.get(i).getNumeroDestinos() == n)
					text = text + "\n" + listaItinerarios.get(i).toString();
		
		return text;
	}
	
	/**
	 * Calcula el numero maximo de destinos
	 * @return el numero maximo de destinos
	 */
	private int getMax() {
		int max = 0;
		
		for (int i = 0; i < listaItinerarios.size(); i++)
			if(listaItinerarios.get(i).getNumeroDestinos() > max)
				max = listaItinerarios.get(i).getNumeroDestinos();
		
		return max;
	}
	
	/**
	 * Mostrará el contenido de los itinerario por orden descendente según el número de destinos.
	 * @return una String con los itinerario
	 */
	public String OrderLowToHigh() {
		int min = getMin();
		String text = "";
		
		for (int n = min; n <= getMax() ; n++)
			for (int i = 0; i < listaItinerarios.size(); i++)
				if(listaItinerarios.get(i).getNumeroDestinos() == n)
					text = text + "\n" + listaItinerarios.get(i).toString();
		
		return text;
	}
	
	/**
	 * Calcula el numero minimo de destinos
	 * @return el numero minimo de destinos
	 */
	private int getMin() {
		int min = Integer.MAX_VALUE;
		
		for (int i = 0; i < listaItinerarios.size(); i++)
			if(listaItinerarios.get(i).getNumeroDestinos() < min)
				min = listaItinerarios.get(i).getNumeroDestinos();
		
		return min;
	}
	
	/**
	 * Calcula el destino mas comun, no distingue mayusculas y minusculas
	 * @return el destino mas comun o "No existen destinos"
	 */
	public String destinoComun() {
		ArrayList<String> allDestinos = getAllDestinos();
		
		if(allDestinos.size() == 0)
			return "No existen destinos";
		
		String result = allDestinos.get(0);
		int maxRepeticiones = 0;
		
		for (int i = 0; i < allDestinos.size(); i++) {
			 int repeticiones = 0;
			 for (int n = i+1; n < allDestinos.size(); n++)
				 if(allDestinos.get(i).equalsIgnoreCase(allDestinos.get(n)))
					 repeticiones++;
			 if(repeticiones > maxRepeticiones) {
				 maxRepeticiones = repeticiones;
				 result = allDestinos.get(i);	
			 }
		}
		return result;
	}
	
	/**
	 * Calcula todos los destinos de todos los itinerario
	 * @return una array con todos los destinos de todos los itinerario
	 */
	private ArrayList<String> getAllDestinos(){
		ArrayList<String> destinos =  new ArrayList<String>();
		for (int i = 0; i < listaItinerarios.size(); i++)
			for (int n = 0 ; n < listaItinerarios.get(i).getNumeroDestinos(); n++)
				destinos.add(listaItinerarios.get(i).getDestino(n));
		
		return destinos;
	}
	
	/**
	 * Devuelve la lista de itinerario
	 * @return Devuelve un String con la lista de itinerario 
	 */
	@Override
	public String toString() {
		return 	"______________________\n" +
				"|lista de Itinerarios|\n" +
				"______________________\n" + listaItinerarios.toString();
	}
}
