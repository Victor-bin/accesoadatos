/**
 * 
 */
package ejercicioAeropuerto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clase del menu, contiene la funcion main
 * @author vtienza
 * @version 1.2
 */
public class Main {

	/**
	 * @param args	main arguments, este programa no trabaja con ellos
	 */
	public static void main(String[] args) {
		final Scanner leerTeclado = new Scanner(System.in);
		String titulo = 
				"________________________\n"	+
				"|Maquina de los numeros|\n" +
			 	"________________________\n";
		String instruccion = 
				"Las opciones son: \n "
			+	"1  -> Leer fichero.\n "
			+	"2  -> Insertar.\n "
			+	"3  -> Borrar.\n "
			+	"4  -> Vaciar.\n "
			+   "5  -> Modificar.\n "
			+	"6  -> Guardar fichero.\n "
			+	"7  -> Separar fichero.\n "
			+	"8  -> Mostrar contenido.\n "
			+	"9  -> Mostrar itinerarios de menor a mayo.\n "
			+	"10 -> Mostrar itinerarios de mayor a menor.\n "
			+	"11 -> Mostrar el destino más repetido de todos los itinerarios.\n "
			+   "12 -> Salir.\n ";
			
		int opcion = 0;
		int SALIR = 12;//valor de salida


		String nombre;
		int numeroDestinos;
		ArrayList<String> destinos;
		
		ListaItinerarios lista = new ListaItinerarios();
		FicheroItinerarios fichero;
		
		System.out.println(titulo);
		System.out.println(instruccion);

		try {
			opcion = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			leerTeclado.next();
		}

				
		// bucle menu principal
		do {
			switch (opcion) {
			case 1:// Leer fichero
				System.out.print("Intoduce el fichero a leer: ");
				String file = leerTeclado.next();
				
				fichero = new FicheroItinerarios(file);
				fichero.abrir();
				
				try {
					while (!fichero.finalFichero()) {
						Itinerario itinerario = new Itinerario();
						fichero.leer(itinerario);
						lista.insertar(itinerario);
					}
					fichero.cerrar();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
				
			break;
			
			case 2:// Insertar
				System.out.println("DATOS DEL ITINERARIO");
				
				System.out.print("Nombre: ");
				nombre = leerTeclado.next();
				
				System.out.print("Numero de destinos: ");
				numeroDestinos = leerTeclado.nextInt();
				
				destinos = new ArrayList<String>();
				System.out.println("DESTINOS ");
				for (int i = 0; i < numeroDestinos; i++) {
					System.out.print("Destino " + i + " : ");
					destinos.add(leerTeclado.next());
				}
				
				lista.insertar(new Itinerario(nombre, destinos));
				
			break;
			
			case 3:// Borrar
				for (int i = 0; i < lista.getLongitud(); i++)
					System.out.println("INTINERARIO " + i + " :\n" + lista.getItinerario(i).toString());
					
				System.out.print("Numero del intinerario a borrar");
				int posicion = leerTeclado.nextInt();
				lista.borrar(lista.getItinerario(posicion));
				
			break;
			
			case 4:// Vaciar
				lista.vaciar();
				
			break;
			
			case 5:// Modificar
				for (int i = 0; i < lista.getLongitud(); i++)
					System.out.println("Numero " + i + " " + lista.getItinerario(i).toString() + "\n");
				System.out.print("Numero del intinerario a Cambiar: ");
				Itinerario 	actual = lista.getItinerario(leerTeclado.nextInt());
			
				System.out.println("DATOS A MODIFICAR INTINERARIO");
				System.out.println("1 -> Nombre\n"
								  +"2 -> Agregar destino\n"
								  +"3 -> Quitar destino\n");
				String cambio;
				switch (leerTeclado.nextInt()) {
				case 1:
					System.out.print("Nombre: ");
					cambio = leerTeclado.next();
					lista.modificar(actual,cambio,Operacion.MODNAME);
					break;
				case 2:
					
					System.out.print("Nuevo destino: ");
					cambio = leerTeclado.next();
					lista.modificar(actual,cambio,Operacion.ADDDESTINY);
					break;
					
				case 3:
					numeroDestinos = actual.getNumeroDestinos();
					System.out.println("Destinos: ");
					for (int i = 0; i < numeroDestinos; i++)
						System.out.print(actual.getDestino(i) + "\n");
					System.out.print("numero del destino a borrar: ");
					int destinoBorrar = leerTeclado.nextInt();
					cambio = actual.getDestino(destinoBorrar);
					lista.modificar(actual,cambio,Operacion.RMVDESTINY);
					break;
					
				default:
					System.out.println("valor no valido");
					break;
				};

			break;
			
			case 6:// Guardar fichero
				System.out.print("Intoduce el fichero a leer: ");
				file = leerTeclado.next();
				
				fichero = new FicheroItinerarios(file);
				fichero.abrir();
				
				for (int i = 0; i < lista.getLongitud(); i++)
					fichero.escribir(lista.getItinerario(i));
					
				try {
					fichero.cerrar();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}	
			break;
			
			case 7:// Separar ficheros 
				System.out.print("El numero :");
				int num =  leerTeclado.nextInt();
				String min, max;
				System.out.print("Intoduce el fichero para los menores a "+num+": ");
				min = leerTeclado.next();
				System.out.print("Intoduce el fichero para los iguales y mayores a "+num+": ");
				max = leerTeclado.next();
				FicheroItinerarios fichero1 = new FicheroItinerarios(min);
				FicheroItinerarios fichero2 = new FicheroItinerarios(max);
				
				fichero1.abrir();
				fichero2.abrir();
				
				for (int i = 0; i < lista.getLongitud(); i++)
					if (lista.getItinerario(i).getNumeroDestinos() < num) 
						fichero1.escribir(lista.getItinerario(i));
					else
						fichero2.escribir(lista.getItinerario(i));
				
				try {
					fichero1.cerrar();
					fichero2.cerrar();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
				
			break;
			
			case 8:// Mostrar contenido
				System.out.println(lista.toString());
			break;
			
			case 9:// Mostrar itinearios de menor a mayo
				System.out.println(lista.OrderLowToHigh());
			break;
			
			case 10:// Mostrar itinearios de mayor a menor
				System.out.println(lista.OrderHighToLow());
			break;
			
			case 11:// Mostrar el destino más repetido de todos los itinerario
				System.out.println(lista.destinoComun());
			break;
			
			default:
				System.out.println("valor invalido");
			break;
			}
			if (opcion != SALIR ) {// si no ha acabado
				System.out.println("Desea continuar? " + instruccion);// repite las opciones
				try {
					opcion = leerTeclado.nextInt();// lee la opcion
				} catch (Exception e) {
					System.out.println("valor invalido");
					leerTeclado.next();
					opcion = 422;//resetea la opcion
				}
			} 
		} while (opcion != SALIR );
		
		leerTeclado.close();
	}
}
