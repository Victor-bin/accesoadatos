/**
 * 
 */
package ejercicioAeropuerto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.AccessException;
import java.util.ArrayList;
import java.util.regex.Pattern;


/**
 * El objeto FicheroItinerarios y sus metodos permiten 
 * crear, gestionar, leer y escribir ficheros con informacion de objetos Intinerario
 * @author vtienza
 * @version 1.0
 */
public class FicheroItinerarios {
	/**
	 * El nombre del fichero a tratar
	 */
	private String nombreFichero;
	/**
	 * El canal de escritura con el fichero
	 */
	private BufferedWriter ficheroW;
	/**
	 *  El canal de lectura con el fichero
	 */
	private BufferedReader ficheroR;
	/**
	 * Array de lineas a tratar
	 */
	private ArrayList<String> lineasLeidas;
	
	
	/**
	 * El constructor solo requiere el nombre, los buffers se pondran nulos y la array se instanciara
	 * @param nombreFichero			Nombre del fichero a leer o escribir
	 */
	public FicheroItinerarios(String nombreFichero) {
		this.nombreFichero = nombreFichero;
		ficheroW = null;
		ficheroR = null;
		lineasLeidas = new ArrayList<String>();
	}
	
	/**
	 * Instancia los buffers al fichero con el nombre introducido en el constructor.<br>
	 * Si el fichero no existe, se creará vacío.<br>
	 * Se procederá a abrir el fichero para realizar operaciones sobre él.
	 */
	public void abrir(){
		lineasLeidas.clear();
		File file = new File(nombreFichero);
		try {
			if (!file.getAbsoluteFile().exists())
				file.createNewFile();
			ficheroW = new BufferedWriter(new FileWriter(file,true));		
			ficheroR = new BufferedReader(new FileReader(file));	
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}		
	}
	
	/**
	 * Lee del fichero un itinerario y lo guarda
	 * @throws IOException si el fichero no esta abierto, no se puede leer o es el fin del fichero
	 * @param itinerario El Intinerario a rellenar
	 */
	public void leer(Itinerario itinerario) throws IOException  {
		
		if (ficheroR == null)
			throw new IOException("EL fichero no esta abierto");
		
		if (lineasLeidas.isEmpty())
			if(finalFichero())
				throw new IOException("Fin del fichero");
		
		
		String separador = Pattern.quote("|");
		String linea = null;
		//String nombre;
		int numDestinos;
		//ArrayList<String> destinos = new ArrayList<String>();
			
		linea = lineasLeidas.get(0);
		lineasLeidas.remove(0);
		String[] argumentos = linea.split(separador);
		itinerario.setNombre(argumentos[0]);
		numDestinos = Integer.parseInt(argumentos[1]);
		for (int i = 0; i < numDestinos; i++) 
			itinerario.addDestino(argumentos[i+2]);
	}
	
	/**
	 * Al llamar a este método, escribirá en el fichero  el itinerario indicado.
	 * @param itinerario		El itinerario a escribir
	 * @return <b>true</b> si lo escribe, <b>false</b> en caso contrario
	 */
	public boolean escribir(Itinerario itinerario){
		try {
			if (ficheroW == null)
				throw new AccessException("EL fichero no esta abierto");
			
			String cadenaItinerario;
			
			cadenaItinerario = itinerario.getNombre()+"|"+itinerario.getNumeroDestinos();
				for (int i = 0; i < itinerario.getNumeroDestinos(); i++)
					cadenaItinerario += "|"+itinerario.getDestino(i);
				
			ficheroW.write(cadenaItinerario);
			ficheroW.newLine();
			ficheroW.flush();
			return true;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
		
	}
	
	/**
	 * Al  llamar  a  este  método, se  cerrará  el  fichero  previamente  abierto.
	 * @throws IOException indicando que el fichero no se encuentra abierto.
	 */
	public void cerrar() throws IOException{
		ficheroR.close();
		ficheroW.close();
	}
	
	/**
	 * Comprueba el final del fichero
	 * @return devolverá <b>true</b> cuando se haya alcanzado el final del fichero. En caso contrario, devolverá <b>false</b>.
	 */
	public boolean finalFichero(){
		try {
			lineasLeidas.add(ficheroR.readLine());
			int leido = lineasLeidas.size() - 1;
			if (lineasLeidas.get(leido) != null)
				return false;
			else
				lineasLeidas.remove(leido);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		return true;

	}
	
	/**
	 *  Se borrará el contenido del fichero
	 */
	public void vaciar() {
		BufferedWriter bw;
		try {
			if (ficheroW == null)
				throw new AccessException("EL fichero no esta abierto");
			bw = new BufferedWriter(new FileWriter(nombreFichero));
			bw.write("");
			bw.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
	}
}