/**
 * 
 */
package ejercicioAeropuerto;

/**
 * Operaciones del metodo modificar
 * @author vtienza
 *
 */
public enum Operacion {
	/**
	 * La operacion de modificar un nombre
	 */
	MODNAME,
	/**
	 * La operacion de añadir un destino
	 */
	ADDDESTINY,
	/**
	 * La operacion de borrar un destino
	 */
	RMVDESTINY
}
