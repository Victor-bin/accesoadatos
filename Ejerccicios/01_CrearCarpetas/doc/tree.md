- proyecto
    - estilos
        - css4
            - estilo1.css
            - estilo2.css
        - css3
            - estilo3.css
    - script
        - js
            - script1.js
            - script2.js
        - php
            - cabecera.php
            - modelo.php
    - imagenes
        - alta
            - paisaje.jpg
        - media
            - paisaje.jpg
        - baja
            - paisaje.jpg
