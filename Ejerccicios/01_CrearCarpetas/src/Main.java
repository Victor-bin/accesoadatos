package ejercicio4TrabajoConFicheros;
/**
 * @author vtienza
 * this program creates the directory and file tree described in 
 * <a>https://gitlab.com/Victor-bin/accesoadatos/-/blob/master/Ejerccicios/01_CrearCarpetas/doc/tree.md</a>
 */
import java.io.*;
public class Main 
{
	public static void main(String[] args) 
	{
		File filesToCreate[] = 
			{
			new File("proyecto/estilos/css4"),
			new File("proyecto/estilos/css3"),
			new File("proyecto/script/js"),
			new File("proyecto/script/php"),
			new File("proyecto/imagenes/alta"),
			new File("proyecto/imagenes/media"),
			new File("proyecto/imagenes/baja"),
			new File("proyecto/estilos/css4","estilo1.css"),
			new File("proyecto/estilos/css4","estilo2.css"),
			new File("proyecto/estilos/css3","estilo3.css"),
			new File("proyecto/script/js","script1.js"),
			new File("proyecto/script/js","script2.js"),
			new File("proyecto/script/php","cabecera.php"),
			new File("proyecto/script/php","modelo.php"),
			new File("proyecto/imagenes/alta","paisaje.jpg"),
			new File("proyecto/imagenes/media","paisaje.jpg"),
			new File("proyecto/imagenes/baja","paisaje.jpg")
			};
		
		for (int i = 0; i < filesToCreate.length; i++) 
		{
			if (!filesToCreate[i].getName().contains(".")) // check if it has an extension, if not, it considers a directory
			{ 
				if(!filesToCreate[i].mkdirs()) // Create the directories from which it hangs if necessary
					System.err.println("El direcctorio ya existe"); // if returns false the target directory already exists
			} else
				try { 
						filesToCreate[i].createNewFile(); // if has extension, it considers a file
					} 
				catch (IOException e) 
					{ 
						System.err.println("Error al crear el fichero " + e.getMessage()); 
					}	
		}
	}
}