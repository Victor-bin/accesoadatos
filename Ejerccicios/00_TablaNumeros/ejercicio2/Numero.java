package ejercicio2;

/**
 * @author vtienza
 *
 */
public class Numero {
	private Integer value;

	public Numero(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
	
	public String toString() {
		return "\n\t" + value + "";
	}

}
