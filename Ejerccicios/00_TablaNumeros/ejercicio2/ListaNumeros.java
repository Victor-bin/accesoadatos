package ejercicio2;

import java.util.ArrayList;
import java.util.List;

public class ListaNumeros {
	private static List<Numero> listaNumeros = new ArrayList<Numero>();
	
	
	public int getNumerosAlmacenados() {
		return listaNumeros.size();
	}

	public void añadirNumero() {
		int numeroPedido = pedirNumero();
		Numero numero = new Numero(numeroPedido);
		listaNumeros.add(numero);
	}	
	
	public void borrarNumero() {
		
		System.out.println("Borrado de numeros\n"
						+  "__________________");
		int numeroPedido = pedirNumero();
		Numero numero = null;
		
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(numeroPedido == listaNumeros.get(i).getValue())
			  numero = listaNumeros.get(i);
		}
	
		if(numero != null)
			listaNumeros.remove(listaNumeros.indexOf(numero));
		else
			System.err.println("El numero no esta en la lista");

	}

	public int mayorNumero(){
		int Mayor = Integer.MIN_VALUE;
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(Mayor < listaNumeros.get(i).getValue());
				Mayor = listaNumeros.get(i).getValue();
		}
		return Mayor;
	}
	
	public int menorNumero(){

		Integer Menor = Integer.MAX_VALUE;
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(Menor > listaNumeros.get(i).getValue())
				Menor = listaNumeros.get(i).getValue();
		}
		return Menor;
	}

	private static int pedirNumero(){
		System.out.println("Intoduzca un numero");
		int num;
		try {
			num = Ejercicio2.leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			Ejercicio2.leerTeclado.nextLine();
			num = pedirNumero();
		}
	return num;
	}
	
	@Override
	public String toString() {
		return "ListaNumeros " + listaNumeros.toString();
	}

}
