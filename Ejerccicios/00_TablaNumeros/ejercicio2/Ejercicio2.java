package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {
	
	static final Scanner leerTeclado = new Scanner(System.in);
	
	static String titulo = 
			"Maquina de los numeros\n"
		 + 	"_________________";
	static String instruccion = 
			"Las opciones para introducir son: \n "
		+	"1-> Meter número.\n "
		+	"2-> Mostrar números.\n "
		+	"3-> Mostrar el número más alto.\n "
		+	"4-> Mostrar el número más bajo.\n "
		+   "5-> Borrar un numero.\n "
		+   "0-> Salir.\n ";
	
	static int opcion = -1;
	static int SALIR = 0;//valor de salida


	public static void main(String[] args) {
		ListaNumeros listaNumeros = new ListaNumeros();
		System.out.println(titulo);
		listaNumeros.añadirNumero();
		System.out.println(instruccion);
		// TODO Auto-generated method stub
		
		// lectura opcion
		try {
			opcion = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			leerTeclado.next();
		}

		
		// bucle menu principal
		do {
			switch (opcion) {
			case 1:
				listaNumeros.añadirNumero();
			break;
			case 2:
				System.out.println(listaNumeros.toString());
			break;
			case 3:
				System.out.println(listaNumeros.mayorNumero());
			break;
			case 4:
				System.out.println(listaNumeros.menorNumero());
			break;
			case 5:
				listaNumeros.borrarNumero();
			break;
			default:
				System.out.println("valor invalido");
				break;
			}
			if (opcion != SALIR ) {// si no ha acabado
				System.out.println("Desea continuar? " + instruccion);// repite las opciones
				try {
					opcion = leerTeclado.nextInt();// lee la opcion
				} catch (Exception e) {
					System.out.println("valor invalido");
					leerTeclado.next();
					opcion = 422;//resetea la opcion
				}
			} 
		} while (opcion != SALIR );// mientra la bandera este bajada
		leerTeclado.close();
	}
}
