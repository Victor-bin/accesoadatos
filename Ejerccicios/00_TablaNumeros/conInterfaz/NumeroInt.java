package ejercicio3;

/**
 * @author vtienza
 *
 */
public class NumeroInt extends Numero{

	public NumeroInt(int value) {
		super((double) value);
	}
	@Override
	public double getValue() {
		return super.getValue();
	}
	
	public void setValue(int value) {
		super.setValue((double) value);
	}
	
	@Override
	public String toString() {
		return "\n\t      " + super.toString();
	}

}
