package ejercicio3;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interfaze {
	
	ListaNumeros listaNumeros = new ListaNumeros();

	protected JFrame frmTablaNumeros;
	private JTextField tflEntrada;
	private JTextField tflSalida;

	/**
	 * Create the application.
	 */
	
	public Interfaze() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTablaNumeros = new JFrame();
		frmTablaNumeros.setTitle("La tabla de los números");
		frmTablaNumeros.setBounds(100, 100, 730, 355);
		frmTablaNumeros.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTablaNumeros.getContentPane().setLayout(null);
		
		JButton btnMeterNmr = new JButton("Meter número");
		btnMeterNmr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Numero num = new Numero(Double.parseDouble(tflEntrada.getText()));
					listaNumeros.añadirNumero(num);
					tflSalida.setText("Numero " + num + " introducido.");
				} catch (Exception e2) {
					tflSalida.setText("Campo Vacio o invalido: " + e2.getMessage());
				}
				tflEntrada.setText("");
			}
		});
		btnMeterNmr.setBounds(12, 31, 159, 25);
		frmTablaNumeros.getContentPane().add(btnMeterNmr);
		
		JButton btnMostrarNmr = new JButton("Mostrar números");
		btnMostrarNmr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tflSalida.setText(listaNumeros.toString());
			}
		});
		btnMostrarNmr.setBounds(12, 128, 159, 25);
		frmTablaNumeros.getContentPane().add(btnMostrarNmr);
		
		JButton btnMostrarMayr = new JButton("Mostrar el mayor");
		btnMostrarMayr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listaNumeros.getNumerosAlmacenados() > 0)
					tflSalida.setText("El mayor numero es: " + listaNumeros.mayorNumero().toString());
				else
					tflSalida.setText("Primero introduce un numero");
				tflEntrada.setText("");
			}
		});
		btnMostrarMayr.setBounds(559, 128, 159, 25);
		frmTablaNumeros.getContentPane().add(btnMostrarMayr);
		
		JButton btnMostrarMenr = new JButton("Mostrar el menor");
		btnMostrarMenr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listaNumeros.getNumerosAlmacenados() > 0)
					tflSalida.setText("El menor numero es: " + listaNumeros.menorNumero().toString());
				else
					tflSalida.setText("Primero introduce un numero");
				tflEntrada.setText("");
			}
		});
		btnMostrarMenr.setBounds(281, 128, 159, 25);
		frmTablaNumeros.getContentPane().add(btnMostrarMenr);
		
		JButton btnBorrarNmr = new JButton("Borrar número");
		btnBorrarNmr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Numero num = new Numero(Double.parseDouble(tflEntrada.getText()));
					if(listaNumeros.borrarNumero(num))
						tflSalida.setText("Numero " + num + " borrado.");
					else {
						tflSalida.setText("Numero no encontrado.");
					}
				} catch (Exception e2) {
					tflSalida.setText("Campo Vacio o invalido: " + e2.getMessage());
				}
				tflEntrada.setText("");
			}
		});
		btnBorrarNmr.setBounds(559, 31, 159, 25);
		frmTablaNumeros.getContentPane().add(btnBorrarNmr);
		
		tflEntrada = new JTextField();
		tflEntrada.setToolTipText("Intoduzca aqui el número");
		tflEntrada.setBounds(281, 23, 159, 41);
		frmTablaNumeros.getContentPane().add(tflEntrada);
		tflEntrada.setColumns(10);
		
		tflSalida = new JTextField();
		tflSalida.setEditable(false);
		tflSalida.setBounds(12, 192, 706, 103);
		frmTablaNumeros.getContentPane().add(tflSalida);
		tflSalida.setColumns(10);
		frmTablaNumeros.setLocationRelativeTo(null);
	}
}
