package ejercicio2v2;

import java.util.Scanner;

public class Ejercicio2 {
	
	static final Scanner leerTeclado = new Scanner(System.in);
	
	static String titulo = 
			"Maquina de los numeros\n"
		 + 	"_________________";
	static String instruccion = 
			"Las opciones para introducir son: \n "
		+	"1-> Meter número decimal largo.\n "
		+	"2-> Mostrar números.\n "
		+	"3-> Mostrar el número más alto.\n "
		+	"4-> Mostrar el número más bajo.\n "
		+   "5-> Borrar un numero.\n "
		+	"6-> Meter número entero.\n "
		+	"7-> Meter número decimal.\n "
		+   "0-> Salir.\n ";
	
	static int opcion = -1;
	static int SALIR = 0;//valor de salida


	public static void main(String[] args) {
		ListaNumeros listaNumeros = new ListaNumeros();
		System.out.println(titulo);
		listaNumeros.añadirNumero(pedirNumero());
		System.out.println(instruccion);
		// TODO Auto-generated method stub
		
		// lectura opcion
		try {
			opcion = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			leerTeclado.next();
		}

		
		// bucle menu principal
		do {
			switch (opcion) {
			case 1:
				listaNumeros.añadirNumero(pedirNumero());
			break;
			case 2:
				System.out.println(listaNumeros);//.toString() es omisible//
			break;
			case 3:
				System.out.println(listaNumeros.mayorNumero());
			break;
			case 4:
				System.out.println(listaNumeros.menorNumero());
			break;
			case 5:
				System.out.println("Borrado de numeros\n"
						+  "__________________");
				listaNumeros.borrarNumero(pedirNumero());
			break;
			case 6:
				listaNumeros.añadirNumero(pedirNumeroInt());
			break;
			case 7:
				listaNumeros.añadirNumero(pedirNumeroFloat());
			break;
			default:
				System.out.println("valor invalido");
				break;
			}
			if (opcion != SALIR ) {// si no ha acabado
				System.out.println("Desea continuar? " + instruccion);// repite las opciones
				try {
					opcion = leerTeclado.nextInt();// lee la opcion
				} catch (Exception e) {
					System.out.println("valor invalido");
					leerTeclado.next();
					opcion = 422;//resetea la opcion
				}
			} 
		} while (opcion != SALIR );// mientra la bandera este bajada
		leerTeclado.close();
	}
	
	private static Numero pedirNumero(){
		System.out.println("Intoduzca un numero");
		Numero num = null;
		try {
			num = new Numero(leerTeclado.nextDouble());// lee la opcion
		} catch (Exception e) {
				leerTeclado.nextLine();
			num = pedirNumero();
		}
	return num;
	}
	
	private static NumeroInt pedirNumeroInt(){
		System.out.println("Intoduzca un numero");
		NumeroInt num = null;
		try {
			num = new NumeroInt(leerTeclado.nextInt());// lee la opcion
		} catch (Exception e) {
				leerTeclado.nextLine();
			num = pedirNumeroInt();
		}
	return num;
	}
	
	private static NumeroFloat pedirNumeroFloat(){
		System.out.println("Intoduzca un numero");
		NumeroFloat num = null;
		try {
			num = new NumeroFloat(leerTeclado.nextFloat());// lee la opcion
		} catch (Exception e) {
				leerTeclado.nextLine();
			num = pedirNumeroFloat();
		}
	return num;
	}
}
