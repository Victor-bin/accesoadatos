package ejercicio2v2;

/**
 * @author vtienza
 *
 */
public class NumeroFloat extends Numero{

	public NumeroFloat(float value) {
		super((double) value);
	}
	@Override
	public double getValue() {
		return super.getValue();
	}
	
	public void setValue(float value) {
		super.setValue((double) value);
	}
	
	@Override
	public String toString() {
		return "\n\t      " + super.toString();
	}

}
