package ejercicio2v2;

/**
 * @author vtienza
 *
 */
public class Numero {
	private double value;

	public Numero(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	
	public String toString() {
		return "\n\t      " + value;
	}

}
