package ejercicio2v2;

import java.util.ArrayList;
import java.util.List;

public class ListaNumeros {
	private static List<Numero> listaNumeros = new ArrayList<Numero>();
	
	
	public int getNumerosAlmacenados() {
		return listaNumeros.size();
	}

	public void añadirNumero(Numero numero) {
		listaNumeros.add(numero);
	}	
	
	public void borrarNumero(Numero numeroPedido) {
		Numero numero = null;
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(numeroPedido.getValue() == listaNumeros.get(i).getValue())
			  numero = listaNumeros.get(i);
		}
	
		if(numero != null)
			listaNumeros.remove(listaNumeros.indexOf(numero));
		else
			System.err.println("El numero no esta en la lista");

	}

	public Numero mayorNumero(){
		Numero Mayor = new Numero(Integer.MIN_VALUE);
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(Mayor.getValue() < listaNumeros.get(i).getValue());
				Mayor = listaNumeros.get(i);
		}
		return Mayor;
	}
	
	public Numero menorNumero(){
		Numero Menor = new Numero(Integer.MAX_VALUE);
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(Menor.getValue() > listaNumeros.get(i).getValue())
				Menor = listaNumeros.get(i);
		}
		return Menor;
	}

	
	
	@Override
	public String toString() {
		return "ListaNumeros " + listaNumeros.toString();
	}

}
