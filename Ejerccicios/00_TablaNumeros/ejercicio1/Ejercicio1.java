package ejercicio1;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Ejercicio1 {
	
	private static Scanner leerTeclado = new Scanner(System.in);
	
	static String titulo = 
			"Maquina de los numeros\n"
		 + 	"_________________";
	static String instruccion = 
			"Las opciones para introducir son: \n "
		+	"1-> Meter número.\n "
		+	"2-> Mostrar números.\n "
		+	"3-> Mostrar el número más alto.\n "
		+	"4-> Mostrar el número más bajo.\n "
		+   "5-> Borrar un numero.\n "
		+   "0-> Salir.\n ";
	
	static int opcion = -1;
	static int SALIR = 0;//valor de salida


	public static void main(String[] args) {
		List<Integer> listaNumeros = new ArrayList<Integer>();
		System.out.println(titulo);
		listaNumeros.add(PedirNumero());
		System.out.println(instruccion);
		// TODO Auto-generated method stub
		
		// lectura opcion
		try {
			opcion = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			leerTeclado.next();
		}

		
		// bucle menu principal
		do {
			switch (opcion) {
			case 1:
				listaNumeros.add(PedirNumero());
			break;
			case 2:
				System.out.println(listaNumeros.toString());
			break;
			case 3:
				System.out.println(MayorNumero(listaNumeros));
			break;
			case 4:
				System.out.println(MenorNumero(listaNumeros));
			break;
			case 5:
				listaNumeros = BorrarNumero(listaNumeros);
			break;
			default:
				System.out.println("valor invalido");
				break;
			}
			if (opcion != SALIR ) {// si no ha acabado
				System.out.println("Desea continuar? " + instruccion);// repite las opciones
				try {
					opcion = leerTeclado.nextInt();// lee la opcion
				} catch (Exception e) {
					System.out.println("valor invalido");
					leerTeclado.next();
					opcion = 422;//resetea la opcion
				}
			} 
		} while (opcion != SALIR );// mientra la bandera este bajada
		leerTeclado.close();
	}
	
	private static List<Integer> BorrarNumero(List<Integer> listaNumeros) {
		
		System.out.println("Intoduzca el numero a borrar");
		int numero = PedirNumero();
	
		try {
			listaNumeros.remove(listaNumeros.indexOf(numero));
		} catch (Exception e) {
			System.err.println("El numero no esta en la lista");
		}
		
		
		return listaNumeros;
	}

	private static int MayorNumero(List<Integer> listaNumeros){
		Integer Mayor = Integer.MIN_VALUE;
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(Mayor < listaNumeros.get(i))
				Mayor = listaNumeros.get(i);
		}
		return Mayor;
	}
	
	private static int MenorNumero(List<Integer> listaNumeros){

		Integer Menor = Integer.MAX_VALUE;
		for (int i = 0; i < listaNumeros.size(); i++) {
			if(Menor > listaNumeros.get(i))
				Menor = listaNumeros.get(i);
		}
		return Menor;
	}

	private static int PedirNumero(){
		System.out.println("Intoduzca un numero");
		int num;
		try {
			num = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			num = PedirNumero();
		}
	return num;
	}
}
