package ejercicioPersonaFichero;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author vtienza
 *
 */
public class TablaPersonas {
	private List<Persona> listaPersonas;
	
	public TablaPersonas() {
		listaPersonas = new ArrayList<Persona>();
	}
	
	public TablaPersonas(Persona persona) {
		listaPersonas = new ArrayList<Persona>();
		listaPersonas.add(persona);
	}
	
	public TablaPersonas(Persona[] personas) {
		listaPersonas = new ArrayList<Persona>();
		for (int i = 0; i < personas.length; i++)
			listaPersonas.add(personas[i]);
	}

	public String mostrarTabla() {
		String result = "";
		for (Persona persona : listaPersonas)
			result += persona.toString() + "\n";
		return result;
	}
	
	public void vaciarTabla() {
		listaPersonas.clear();
	}
	
	public void añadirPersona(Persona persona) {
		listaPersonas.add(persona);
	}
	
	public void añadirPersonas(Persona[] personas) {
		for (int i = 0; i < personas.length; i++)
			listaPersonas.add(personas[i]);
		
	}
	
	public void guardarEnFichero(String nombreFichero) {
		
		try {
			// create
			BufferedWriter ficheroBW = new BufferedWriter(new FileWriter(new File(nombreFichero)));
			
			// write
			for (Persona persona : listaPersonas) {
				ficheroBW.write(persona.getNombre());
				ficheroBW.newLine();
				ficheroBW.write(persona.getApellidos());
				ficheroBW.newLine();
				ficheroBW.write(Integer.toString(persona.getEdad()));
				ficheroBW.newLine();
				ficheroBW.write(persona.getDni());
				ficheroBW.newLine();
			}
			
			// close
			ficheroBW.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}		
	}
	
	public void leerDeFichero(String nombreFichero) {
		try {
			// create
			BufferedReader ficheroBR = new BufferedReader(new FileReader(new File(nombreFichero)));
			// read
			String nombre = ficheroBR.readLine();
			String apellidos = ficheroBR.readLine();
			String edad = ficheroBR.readLine();
			String dni = ficheroBR.readLine();
			Persona line = null;
			
			while (dni != null){
				//System.out.println(nombre + apellidos + edad + dni);
				line = new Persona(nombre, apellidos,Integer.parseInt(edad), dni);
				listaPersonas.add(line);
				nombre = ficheroBR.readLine();
				apellidos = ficheroBR.readLine();
				edad = ficheroBR.readLine();
				dni = ficheroBR.readLine();				
			}
			
			// close
			ficheroBR.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}		
	}

	@Override
	public String toString() {
		return "TablaPersonas [listaPersonas=" + listaPersonas + "]";
	}	
	

}
