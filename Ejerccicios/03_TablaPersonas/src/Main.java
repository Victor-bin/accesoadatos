package ejercicioPersonaFichero;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		final Scanner leerTeclado = new Scanner(System.in);
		String titulo = 
			"Maquina de los numeros\n"
			 + 	"_________________";
		String instruccion = 
				"Las opciones son: \n "
			+	"1-> Mostrar tabla.\n "
			+	"2-> Vaciar la tabla.\n "
			+	"3-> Añadir una persona.\n "
			+	"4-> Añadir varias personas.\n "
			+   "5-> Guardar en un fichero.\n "
			+	"6-> Leer de un fichero.\n "
			+   "0-> Salir.\n ";
			
		int opcion = -1;
		int SALIR = 0;//valor de salida
		//String nombre;
		String nombreFichero;
		TablaPersonas tablaPersonas = new TablaPersonas();
		String nombre;
		String apellido;  
		String dni;
		int edad;
		
		
		System.out.println(titulo);
		System.out.println(instruccion);

		try {
			opcion = leerTeclado.nextInt();// lee la opcion
		} catch (Exception e) {
			leerTeclado.next();
		}

				
		// bucle menu principal
		do {
			switch (opcion) {
			case 1:
				System.out.println("__ NOMBRES __");
				System.out.println(tablaPersonas.mostrarTabla());
			break;
			case 2:
				tablaPersonas.vaciarTabla();
				System.out.println("TABLA BORRADA");
			break;
			case 3:
				System.out.print("Nombre: ");
				nombre = leerTeclado.next();
				System.out.print("apellido: ");
				apellido = leerTeclado.next();
				System.out.print("edad: ");
				edad = leerTeclado.nextInt();
				System.out.print("dni: ");
				dni = leerTeclado.next();
				tablaPersonas.añadirPersona(new Persona(nombre, apellido, edad, dni));;							
			break;
			case 4:
				System.out.print("Numero de personas a introducir: ");
				int n = leerTeclado.nextInt();
				Persona[] personas = new Persona[n];
				for (int i = 0; i < n; i++) {
					System.out.print("Nombre: ");
					nombre = leerTeclado.next();
					System.out.print("apellido: ");
					apellido = leerTeclado.next();
					System.out.print("edad: ");
					edad = leerTeclado.nextInt();
					System.out.print("dni: ");
					dni = leerTeclado.next();
					personas[i] = new Persona(nombre, apellido, edad, dni);
				}
				tablaPersonas.añadirPersonas(personas);
			break;
			case 5:
				System.out.print("Fichero de salida: ");
				nombreFichero = leerTeclado.next();
				tablaPersonas.guardarEnFichero(nombreFichero);
			break;
			case 6:
				System.out.print("Fichero de entrada: ");
				nombreFichero = leerTeclado.next();
				tablaPersonas.leerDeFichero(nombreFichero);
			break;
			default:
				System.out.println("valor invalido");
			break;
			}
			if (opcion != SALIR ) {// si no ha acabado
				System.out.println("Desea continuar? " + instruccion);// repite las opciones
				try {
					opcion = leerTeclado.nextInt();// lee la opcion
				} catch (Exception e) {
					System.out.println("valor invalido");
					leerTeclado.next();
					opcion = 422;//resetea la opcion
				}
			} 
		} while (opcion != SALIR );
		leerTeclado.close();
	}

}
