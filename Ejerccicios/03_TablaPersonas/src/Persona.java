/**
 * 
 */
package ejercicioPersonaFichero;

/**
 * @author vtienza
 *
 */
public class Persona {
	private String nombre;
	private String apellido;
	private int edad;
	private String dni;
	/**
	 * @param nombre
	 * @param apellido
	 * @param edad
	 * @param dni
	 */
	public Persona(String nombre, String apellido, int edad, String dni) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.dni = dni;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellido;
	}
	/**
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	@Override
	public String toString() {
		return nombre + " " + apellido + "\n EDAD: " + edad + "\n DNI: " + dni;
	}


}
